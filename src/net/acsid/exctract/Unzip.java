package net.acsid.exctract;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	List<String> fileList;
	
	//unzip
	
	public void unZipIt(String zipFile, String outputFolder) {
		
		try {
			FileInputStream fis = new FileInputStream(zipFile);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			ZipEntry entry;
			
			//read entry from zipFile
		
			while((entry = zis.getNextEntry()) != null) {
				System.out.println("Unpacking: " + entry.getName());
				File file = new File(entry.getName());
				int size;
				byte[] buffer = new byte[2048];
				if ( entry.isDirectory() ) {
					file.mkdirs();
				}
				else {
					FileOutputStream fos = new FileOutputStream(entry.getName());
				BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length);
				
				while ((size = zis.read(buffer,0,buffer.length)) != -1) {
					bos.write(buffer,0,size);
				}
				System.out.println("Done!");
				bos.flush();
				bos.close();
				}
				
			}
			zis.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
