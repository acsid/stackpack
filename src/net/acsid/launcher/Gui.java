package net.acsid.launcher;

import javax.swing.JFrame;

public class Gui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Gui() {
		setTitle("Acsid Modpack");
		setSize(300,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	public void setVisible(boolean b) {
		setVisible(true);
		
	}
	
}
