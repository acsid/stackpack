package net.acsid.launcher.modpack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingWorker;

public class Manager extends SwingWorker<List<Object>, Void> {
	Ui ui = null;
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String path = null;
	
	//installation method 1(new installation) 2(update)
	private static int method = 0;
	public Manager(Ui ui) {
		this.ui = ui;
	}
	public Manager(Runnable runnable,Ui ui) {
		// TODO Auto-generated constructor stub
		this.ui = ui;
	}
	public void patch() {
		
	    Ui.logText("Set Installation path for the current System.");
		if (isWindows()) {
			path = System.getenv("APPDATA") + "/.minecraft/";
		}
		if (isUnix()) {
			path = System.getProperty( "user.home" ) + "/.minecraft/";
		}
		if ( isMac() ) {
			path = System.getProperty( "user.home" ) + "/Library/Application Support/minecraft/";
		}
		this.ui.progress(1);
		
		Ui.logText("Looking for modpack conf");
		
		File f = new File(path + "config/modpack.cnf");
		 if(f.exists()){
			  method = 1;
		  }else{
			  method = 2;
		  }
		this.ui.progress(1);
		
		if ( method == 1 ) {
			Ui.logText("Patching the modpack ...");
			Config.load(path + "config/modpack.cnf");
			update();
		} else if (method == 2) {
			Ui.logText("Install modpack Patcher...");
			install();
			update();
		}
		
		Config.save(path + "config/modpack.cnf");
		

		
	}
	//isOs
	public static boolean isWindows() {
		 
		return (OS.indexOf("win") >= 0);
 
	}
 
	public static boolean isMac() {
 
		return (OS.indexOf("mac") >= 0);
 
	}
 
	public static boolean isUnix() {
 
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
 
	}
 
	public static boolean isSolaris() {
 
		return (OS.indexOf("sunos") >= 0);
 
	}
	String content = null;
	int totalFile = 0;
	
	List<String> folderContent = new ArrayList<String>();
	List<String> folderContentFinal = new ArrayList<String>();
	
	public void reConfModpack() {
		Ui.logText("Patching mods...");
		this.ui.progress(1);
		  try {
			 content = UrlReader.getText("http://modpack.00stack.in/assets/config");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//this.ui.logText(content);
			Pattern p = Pattern.compile("href=\"(.*?)\"");
			Matcher m = p.matcher(content);
			boolean packfile = false;
			while(m.find()) {
			 if ( m.group(1).equals("/assets/") ) {
				 packfile = true;
				this.ui.progress(1);
			 } else {
				 if (packfile) {
					 Downloader dl = new Downloader("config/"+ m.group(1), path);
				 }
			 }
			}
			 
	}
	
	public void checkFlan() {
		Ui.logText("Patching Flan's mods...");
		this.ui.progress(1);
		
		
		File f = new File(path + "/Flan");
		if (f.exists() && f.isDirectory()) {
		  //laissez vide
		} else {
			f.mkdir();
			
		}
		
		
		
		  try {
			 content = UrlReader.getText("http://modpack.00stack.in/assets/flan");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  this.ui.progress(1);
			//this.ui.logText(content);
			Pattern p = Pattern.compile("href=\"(.*?)\"");
			Matcher m = p.matcher(content);
			boolean packfile = false;
			while(m.find()) {
			 if ( m.group(1).equals("/assets/") ) {
				 packfile = true;
				this.ui.progress(1);
			 } else {
				 if (packfile) {
					 Downloader dl = new Downloader("flan/"+ m.group(1), path);
				 }
			 }
			}
			 
	}
	
	//the update method
	public void update() {
		File current = new File(path + "mods");
		File[] fileList = current.listFiles();
		this.ui.progress(1);
		  try {
			 content = UrlReader.getText("http://modpack.00stack.in/assets/mods");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  this.ui.progress(1);
		//this.ui.logText(content);
		Pattern p = Pattern.compile("href=\"(.*?)\"");
		Matcher m = p.matcher(content);
		boolean packfile = false;
		while(m.find()) {
		 if ( m.group(1).equals("/assets/") ) {
			 packfile = true;
			this.ui.progress(1);
		 } else {
			 if (packfile) {
				this.ui.progress(1);
				folderContent.add(m.group(1));
				File f = new File(path + "mods/" + m.group(1));
				 if(f.exists()){
					  Ui.logText(" OK");
				  }else{
					 // this.ui.logText(" Downloading: " + m.group(1));
					  Ui.logText(" Downloading Mods... ");
					  Downloader dl = new Downloader("mods/"+ m.group(1), path);
					  Ui.logText("DONE!");
				  }
				 totalFile++;

				 this.ui.progress(1);
			 }
		 }
		   
		   //this.ui.logText(m.group(1));
		}
		 for (File file:fileList) {
			 if ( file.isFile() ) {
				 this.ui.progress(1);
				
				 boolean needed = false;
				  for (String s : folderContent) {
					  if ( s.contains(file.getName()) ) {
						  needed = true;
					  } 
					 
					  }
				  if (!needed) {
					  	Ui.logText("Removing: " + file.getName());
						file.delete();  
				  }
				  }
				  
		 }
	
		  
		
	}
	
	public void loadOptional() {
		if (Config.getOptifine()) {
			Ui.logText("optifine");
			Downloader dl = new Downloader("mods/optifine.jar", path, "http://00stack.in/minecraft/assets/optional/");
			Ui.logText("optifine DONE!");
		}
		
	}
	
	//the install method
	public void install() {
		Config.save(path);
		//detect minecraft Launcher
		Config.setLauncher(detectLauncher());
	}
	
	
	public File detectLauncher() {
		Ui.logText("Looking for default Minecraft Launcher...");
		 this.ui.progress(1);
		File f = new File(path + "launcher.jar");
		 if(f.exists()){
			 this.ui.progress(1);
			  Ui.logText("Default launcher found");
			  this.ui.progress(1);
			  Downloader dl = new Downloader("Minecraft.jar", path, "https://s3.amazonaws.com/Minecraft.Download/launcher/");
			  f = new File(path + "Minecraft.jar");
			  return f;
		  }else{
			  this.ui.progress(1);
			  Ui.logText("Default launcher not-found");
			  return Open.file();
		  }
	}
	
	//boot minecraft Launcher
	public void boot() {
		this.ui.progress(1);
		try {
			
			String extension = "";
			int i = Config.getLauncher().lastIndexOf('.');
			if (i > 0) {
			    extension = Config.getLauncher().substring(i+1);
			}
			if (extension.equals("jar")) {
				Process p = Runtime.getRuntime().exec("java -jar " + Config.getLauncher());
			} else {
				Process p = Runtime.getRuntime().exec(Config.getLauncher());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ui.progress(100);
		
	}
	@Override
	protected List<Object> doInBackground() throws Exception {
		// TODO Auto-generated method stub
		
		patch();
		reConfModpack();
		checkFlan();
		loadOptional();
		boot();
		System.exit(0);
		
		return null;
	}
	
}


