package net.acsid.launcher.modpack;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import static javax.swing.GroupLayout.Alignment.CENTER;

	public class Ui extends JFrame{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//Component log = "Initialise Modpack Manager";
		private JProgressBar pbar;
		 JTextPane textpane = null; 
		 private JCheckBox optifine;
		 private JCheckBox autoboot;
		 private JButton ok;
		 private static JLabel label;
		 private Ui  UserInterface;
		 static Manager manager = null;
		 @SuppressWarnings("unchecked")
	    public Ui() {
	   
	    	//JLabel label = new JLabel(log);
//	        label.setFont(new Font("Georgia", Font.PLAIN, 14));
	   
			pbar = new JProgressBar();
		        optifine = new JCheckBox();
		        autoboot = new JCheckBox();
		        label = new JLabel();
		        ok = new JButton();
		 
		        setDefaultCloseOperation(EXIT_ON_CLOSE);
		 
		        optifine.setText("Optifine");
		 
		        autoboot.setText("AutoBoot");
		        label.setHorizontalAlignment(label.CENTER);
	
		        label.setText("ModLoader Version: 0.2.1");
		 
		        ok.setText("OK");
		        
		        ok.addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent evt) {
		        		okBtn(evt);
		        	}
		        });
		        
		        optifine.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent evt) {
		        		setOptifine(evt);
		        	}
		        });
		        autoboot.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent evt) {
		        		setAutoboot(evt);
		        	}
		        });
		 
		        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		        getContentPane().setLayout(layout);
		        layout.setHorizontalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(layout.createSequentialGroup()
		                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		                    .addGroup(layout.createSequentialGroup()
		                        .addContainerGap()
		                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		                            .addComponent(pbar, GroupLayout.DEFAULT_SIZE, 448, GroupLayout.PREFERRED_SIZE)
		                            .addGroup(layout.createSequentialGroup()
		                                .addComponent(optifine)
		                                .addGap(30, 30, 30)
		                              
		                                .addPreferredGap(ComponentPlacement.RELATED, 87, GroupLayout.PREFERRED_SIZE)
		                                .addComponent(ok)
		                                .addGap(22, 22, 22))))
		                    .addGroup(layout.createSequentialGroup()
		                        .addGap(29, 29, 29)
		                        .addComponent(label, GroupLayout.PREFERRED_SIZE, 392, Short.MAX_VALUE)))
		                .addContainerGap())
		        );
		        layout.setVerticalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(layout.createSequentialGroup()
		                .addGap(57, 57, 57)
		                .addComponent(label)
		                .addGap(18, 18, 18)
		                .addComponent(pbar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		                .addGap(18, 18, 18)
		                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
		                    .addComponent(optifine)
		           
		                    .addComponent(ok))
		                .addGap(21, 21, 21))
		        );
		 
		        pack();
		        UserInterface = this;
	     
	    }
	    
	    protected void setAutoboot(ActionEvent evt) {
			// TODO Auto-generated method stub
	    	//Config.setAutoboot();
		}

		protected void setOptifine(ActionEvent evt) {
			Config.setOptifine();
		}
		protected void okBtn(ActionEvent evt) {
	        EventQueue.invokeLater(new Runnable() {
	        	public void run() {
	        		try {
	        			manager = new Manager(this, UserInterface );
	        			runManager = true;
	        		} catch (Exception e) {
	        			// TODO Auto-generated catch block
	        			e.printStackTrace();
	        		}
	        	}
	        	}
	        ); 
		
		}
		
		static boolean runManager = false;
		public static void logText(String str) {
			System.out.println(str);
			label.setText(str);
			
		}
		public static void doTask() {
			if (runManager) {
				try {
					manager.doInBackground();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				runManager = false;
			}
		}

		public void progress(int value) {
	            
                int val = pbar.getValue();
                
                if (val >= 100) {
    
                    return;
                }

                pbar.setValue(val + value);  
                validate();
                repaint();
        }
	    
	    
	
		

	    public static void main(String[] args) {
	                Ui ex = new Ui();
	                ex.setVisible(true); 
	                while (true) {
	                	doTask();
	                	try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                }
	               // System.exit(0);
	      
	    }
	}

