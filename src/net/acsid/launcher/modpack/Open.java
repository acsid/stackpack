package net.acsid.launcher.modpack;

import java.io.File;

import javax.swing.JFileChooser;

public class Open {
	public static File file() {
	JFileChooser fc = new JFileChooser();
	int returnVal = fc.showOpenDialog(null); //Where frame is the parent component	
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    return fc.getSelectedFile();
	    //Now you have your file to do whatever you want to do
	} else {
	    //User did not choose a valid file
		System.exit(0);
	}
	return null;
	}
}
