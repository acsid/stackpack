package net.acsid.launcher.modpack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
 static Properties p = new Properties();
	public static void load(String path) {
		
	      try {
			p.load(new FileInputStream(path));
			p.setProperty("optifine", "0");
			p.setProperty("autoboot", "0");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void save(String path) {
		try {
			p.store(new FileOutputStream(path), "");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void setLauncher(File detectLauncher) {
		// TODO Auto-generated method stub
		p.setProperty("launcher", detectLauncher.getAbsolutePath());
		
		
	}
	
	static boolean optifine = false;
	public static void setOptifine(){
		if (optifine) {
			System.out.println("Dont use optifine");
			optifine = false;
		} else {
			System.out.println("Use Optifine");
			optifine = true;
		}
	}
	
	public static boolean getOptifine() {
		return optifine;
	}
	
	
	public static String getLauncher() {
		return p.getProperty("launcher");
	}
	
}
