package net.acsid.launcher;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.net.URLConnection;
import java.net.URL;
import java.io.InputStream;


public class Main {
	static String outputFolder = "";
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		
		
		
			System.out.println("Acsid Modpack manager V 0.1");
			String download = "http://00stack.in/minecraft/assets/stackmod.jar";
			String file = "stackmod.jar";
			
			String path = null;
			
			if (isWindows()) {
				path = System.getenv("APPDATA") + "/.minecraft/";
			}
			if (isUnix()) {
				path = System.getProperty( "user.home" ) + "/.minecraft/";
			}
			if ( isMac() ) {
				path = System.getProperty( "user.home" ) + "/Library/Application Support/minecraft/";
			}
			
			
			
			
			URLConnection http = null;
			try {
				http = new URL(download).openConnection();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
			InputStream in = null;
			try {
				in = http.getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				System.out.println(path + file);
				OutputStream out = new FileOutputStream(path + file);
				
				try {
					byte[] buf = new byte[512];
					int read;
					while ((read = in.read(buf)) > 0) out.write(buf,0,read);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					try {
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					in.close();
				} catch (IOException e)	 {
					e.printStackTrace();
				}	
			}
			
			
			Process p = Runtime.getRuntime().exec("java -jar " + path + file);
			
			System.out.println("ALL DONE!");
			
			
		
	}
	
	public static boolean isWindows() {
		 
		return (OS.indexOf("win") >= 0);
 
	}
 
	public static boolean isMac() {
 
		return (OS.indexOf("mac") >= 0);
 
	}
 
	public static boolean isUnix() {
 
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
 
	}
 
	public static boolean isSolaris() {
 
		return (OS.indexOf("sunos") >= 0);
 
	}

}
